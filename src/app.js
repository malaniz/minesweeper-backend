const express = require ('express')
const bodyParser = require('body-parser')
const cors = require('cors');

const {getRandomBoard} = require('./minesweeper')
const {saveGamer, getGamers} = require('./storage')

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get('/', (req, res) => res.json({result: 'Api working'}))

app.get('/board', (req, res) => {
  const {width, height, mines} = req.query
  data = getRandomBoard(parseInt(width), parseInt(height), parseInt(mines))
  res.json({data})
})

app.put('/gamer', async (req, res) => {
  const {data:gamer} = req.body
  data = await saveGamer(gamer)
  res.json({data})
})

app.get('/ranking', async (req, res) => {
  data = await getGamers()
  res.json({data})
})

app.use((err, req, res, next) => res.status(500).json({ err }))

module.exports = app
