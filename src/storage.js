const MongoClient = require('mongodb').MongoClient
const {dburl, dbname} = require('../etc/storage.js')

let db = null;
let client = null;

const storeConnect = async () => {
  console.log(`connecting to ${dburl}, ${dbname}`)
  client = await MongoClient.connect(dburl)
  console.log("connected to database")
  db = client.db(dbname);
  return db; 
}

const storeDisconnect = () => {
  client.close()
}

const saveGamer = async (gamerData) => (
  await db.collection('gamer').insertOne(gamerData)
)

const getGamers = async () => (
  await db.collection('gamer').find({}).toArray()
)


db = storeConnect()

module.exports = { saveGamer, getGamers }
