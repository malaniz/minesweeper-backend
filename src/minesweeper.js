const getRandomXY = (width, height) => ({
  x: Math.floor(Math.random() * width),
  y: Math.floor(Math.random() * height),
})

const getRandomBoard = (width, height, amountOfmines) => {
  let board = Array(width).fill(0)
  board = board.map(row => Array(height).fill(0))
  for (let i = 1; i <= amountOfmines; i++) {
    const {x, y} = getRandomXY(width, height)
    if (board[x][y] === 1) {
      i--
      continue
    }
    board[x][y] = 1
  }
  return board;
}


module.exports = {
  getRandomBoard,
}
